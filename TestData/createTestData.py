import random
import json

def obj_dict(obj):
    return obj.__dict__

class Stanje:
    z = 0.0
    longitude = 0.0
    latitude = 0.0

data = list()

for i in range(0, 1000):
    tmp = Stanje()
    tmp.z = random.uniform(5, 20)
    tmp.longitude = random.uniform(13.3, 16.6)
    tmp.latitude = random.uniform(45.4, 46.9)
    data.append(tmp)

with open('testSlaboCestisceData.json', 'w') as outfile:
    json.dump(data, outfile, default=obj_dict)

data = list()

for i in range(0, 1000):
    tmp = Stanje()
    tmp.z = random.uniform(5, 9)
    tmp.longitude = random.uniform(13.3, 16.6)
    tmp.latitude = random.uniform(45.4, 46.9)
    data.append(tmp)

with open('testDobroCestisceData.json', 'w') as outfile:
    json.dump(data, outfile, default=obj_dict)